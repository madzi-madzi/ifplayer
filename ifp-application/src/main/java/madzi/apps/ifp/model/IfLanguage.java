package madzi.apps.ifp.model;

public enum IfLanguage {
    EN,
    DE,
    UA,
    RU;
}
