package madzi.apps.ifp.model.fx;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlySetProperty;
import javafx.beans.property.ReadOnlySetWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import madzi.apps.ifp.model.IfLanguage;
import madzi.apps.ifp.model.IfPlatform;
import madzi.apps.ifp.model.StoryInfo;

public class FxStoryInfo {

    private StoryInfo storyInfo;

    public FxStoryInfo(final StoryInfo storyInfo) {
        this.storyInfo = storyInfo;
    }

    public ReadOnlyStringProperty titleProperty() {
        return new ReadOnlyStringWrapper(storyInfo.getTitle());
    }

    public ReadOnlyStringProperty versionProperty() {
        return new ReadOnlyStringWrapper(storyInfo.getVersion());
    }

    public ReadOnlyStringProperty dateProperty() {
        return new ReadOnlyStringWrapper(storyInfo.getDate());
    }

    public ReadOnlyStringProperty descriptionProperty() {
        return new ReadOnlyStringWrapper(storyInfo.getDescription());
    }

    public ReadOnlySetProperty<String> authorsProperty() {
        return new ReadOnlySetWrapper<>(FXCollections.observableSet(storyInfo.getAuthors()));
    }

    public ReadOnlySetProperty<String> tagsProperty() {
        return new ReadOnlySetWrapper<>(FXCollections.observableSet(storyInfo.getTags()));
    }

    public ReadOnlyObjectProperty<IfLanguage> languageProperty() {
        return new ReadOnlyObjectWrapper<>(storyInfo.getLanguage());
    }

    public ReadOnlyObjectProperty<IfPlatform> platformProperty() {
        return new ReadOnlyObjectWrapper<>(storyInfo.getPlatform());
    }
}
