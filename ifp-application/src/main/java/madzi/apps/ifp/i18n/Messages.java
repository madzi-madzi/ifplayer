package madzi.apps.ifp.i18n;

import java.util.ResourceBundle;

public enum Messages {
    APP_NAME("app.name"),
    APP_VERSION("app.version"),
    APP_TITLE("app.title");

    private final String key;

    private Messages(final String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }

    public String message() {
        return LANG_BUNDLE.getString(key);
    }

    @Override
    public String toString() {
        return message();
    }

    private static final ResourceBundle LANG_BUNDLE = ResourceBundle.getBundle("ifp");

    public static ResourceBundle resourceBundle() {
        return LANG_BUNDLE;
    }
}
