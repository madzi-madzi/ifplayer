Feature: Application services

  Scenario: Library service should search all stories into folder 'stories'
    Given the application folder is 'src/test/resources/root'
     When the application request all stories
     Then the library services should return 3 items
