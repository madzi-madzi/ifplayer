package madzi.apps.ifp.service;

/**
 * Common application service.
 */
public interface ApplicationService {

    /**
     * Initialize service.
     */
    void init();

    /**
     * Finalize service.
     */
    void stop();
}
