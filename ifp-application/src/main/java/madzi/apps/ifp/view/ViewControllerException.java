package madzi.apps.ifp.view;

/**
 * View controllers exception.
 */
public class ViewControllerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public ViewControllerException() {
        super();
    }

    /**
     * View controller exception constructor.
     *
     * @param message the message for exception
     */
    public ViewControllerException(final String message) {
        super(message);
    }

    /**
     * View controller exception constructor.
     *
     * @param throwable the cause of exception
     */
    public ViewControllerException(final Throwable throwable) {
        super(throwable);
    }

    /**
     * View controller exception constructor.
     *
     * @param message the message for exception
     * @param throwable the cause of exception
     */
    public ViewControllerException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
