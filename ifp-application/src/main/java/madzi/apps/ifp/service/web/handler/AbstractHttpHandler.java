package madzi.apps.ifp.service.web.handler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.HttpHandler;

/**
 * Abstract Http Handler with common methods.
 */
public abstract class AbstractHttpHandler implements HttpHandler {

    private static final Logger logger = LoggerFactory.getLogger(AbstractHttpHandler.class);

    /* 1×× Informational */
    protected static final int SC_CONTINUE                              = 100; // Continue
    protected static final int SC_SWITCHING_PROTOCOL                    = 101; // Switching Protocols
    protected static final int SC_PROCESSING                            = 102; // Processing
    /* 2×× Success */
    protected static final int SC_OK                                    = 200; // OK
    protected static final int SC_CREATED                               = 201; // Created
    protected static final int SC_ACCEPTED                              = 202; // Accepted
    protected static final int SC_NON_AUTHORITATIVE_INFORMATION         = 203; // Non-authoritative Information
    protected static final int SC_NO_CONTENT                            = 204; // No Content
    protected static final int SC_RESET_CONTENT                         = 205; // Reset Content
    protected static final int SC_PARTIAL_CONTENT                       = 206; // Partial Content
    protected static final int SC_MULTI_STATUS                          = 207; // Multi-Status
    protected static final int SC_ALREADY_REPORTED                      = 208; // Already Reported
    protected static final int SC_IM_USED                               = 226; // IM Used
    /* 3×× Redirection */
    protected static final int SC_MULTIPLE_CHOICES                      = 300; // Multiple Choices
    protected static final int SC_MOVED_PERMANENTLY                     = 301; // Moved Permanently
    protected static final int SC_FOUND                                 = 302; // Found
    protected static final int SC_SEE_OTHER                             = 303; // See Other
    protected static final int SC_NOT_MODIFIED                          = 304; // Not Modified
    protected static final int SC_USE_PROXY                             = 305; // Use Proxy
    protected static final int SC_TEMPORARY_REDIRECT                    = 307; // Temporary Redirect
    protected static final int SC_PERMANENT_REDIRECT                    = 308; // Permanent Redirect
    /* 4×× Client Error */
    protected static final int SC_BAD_REQUEST                           = 400; // Bad Request
    protected static final int SC_UNAUTHORIZED                          = 401; // Unauthorized
    protected static final int SC_PAYMENT_REQUIRED                      = 402; // Payment Required
    protected static final int SC_FORBIDDEN                             = 403; // Forbidden
    protected static final int SC_NOT_FOUND                             = 404; // Not Found
    protected static final int SC_METHOD_NOT_ALLOWED                    = 405; // Method Not Allowed
    protected static final int SC_NOT_ACCEPTABLE                        = 406; // Not Acceptable
    protected static final int SC_PROXY_AUTHENTICATION_REQUIRED         = 407; // Proxy Authentication Required
    protected static final int SC_REQUEST_TIMEOUT                       = 408; // Request Timeout
    protected static final int SC_CONFLICT                              = 409; // Conflict
    protected static final int SC_GONE                                  = 410; // Gone
    protected static final int SC_LENGTH_REQUIRED                       = 411; // Length Required
    protected static final int SC_PRECONDITION_FAILED                   = 412; // Precondition Failed
    protected static final int SC_PAYLOAD_TOO_LARGE                     = 413; // Payload Too Large
    protected static final int SC_REQUEST_URI_TOO_LONG                  = 414; // Request-URI Too Long
    protected static final int SC_UNSUPPORTED_MEDIA_TYPE                = 415; // Unsupported Media Type
    protected static final int SC_REQUESTED_RANGE_NOT_SATISFIABLE       = 416; // Requested Range Not Satisfiable
    protected static final int SC_EXPECTATION_FAILED                    = 417; // Expectation Failed
    protected static final int SC_I_M_A_TEAPOT                          = 418; // I'm a teapot
    protected static final int SC_MISDIRECTED_REQUEST                   = 421; // Misdirected Request
    protected static final int SC_UNPROCESSABLE_ENTITY                  = 422; // Unprocessable Entity
    protected static final int SC_LOCKED                                = 423; // Locked
    protected static final int SC_FAILED_DEPENDENCY                     = 424; // Failed Dependency
    protected static final int SC_UPGRADE_REQUIRED                      = 426; // Upgrade Required
    protected static final int SC_PRECONDITION_REQUIRED                 = 428; // Precondition Required
    protected static final int SC_TOO_MANY_REQUESTS                     = 429; // Too Many Requests
    protected static final int SC_REQUEST_HEADER_FIELDS_TOO_LARGE       = 431; // Request Header Fields Too Large
    protected static final int SC_CONNECTION_CLOSED_WITHOUT_RESPONSE    = 444; // Connection Closed Without Response
    protected static final int SC_UNAVAILABLE_FOR_LEGAL_REASONS         = 451; // Unavailable For Legal Reasons
    protected static final int SC_CLIENT_CLOSED_REQUEST                 = 499; // Client Closed Request
    /* 5×× Server Error */
    protected static final int SC_INTERNAL_SERVER_ERROR                 = 500; // Internal Server Error
    protected static final int SC_NOT_IMPLEMENTED                       = 501; // Not Implemented
    protected static final int SC_BAD_GATEWAY                           = 502; // Bad Gateway
    protected static final int SC_SERVICE_UNAVAILABLE                   = 503; // Service Unavailable
    protected static final int SC_GATEWAY_TIMEOUT                       = 504; // Gateway Timeout
    protected static final int SC_HTTP_VERSION_NOT_SUPPORTED            = 505; // HTTP Version Not Supported
    protected static final int SC_VARIANT_ALSO_NEGOTIATES               = 506; // Variant Also Negotiates
    protected static final int SC_INSUFFICIENT_STORAGE                  = 507; // Insufficient Storage
    protected static final int SC_LOOP_DETECTED                         = 508; // Loop Detected
    protected static final int SC_NOT_EXTENDED                          = 510; // Not Extended
    protected static final int SC_NETWORK_AUTHENTICATION_REQUIRED       = 511; // Network Authentication Required
    protected static final int SC_NETWORK_CONNECT_TIMEOUT_ERROR         = 599; // Network Connect Timeout Error

    protected static final String PATH_DELIMITER = "/";

    protected byte[] loadPageNotFound() {
        return loadPageByCode(SC_NOT_FOUND);
    }

    protected byte[] loadPageByCode(final int code) {
        return loadResource("/web/" + code + ".html");
    }

    protected byte[] loadResource(final String resourceName) {
        try (var inputStream = this.getClass().getResourceAsStream(resourceName)) {
            return inputStream.readAllBytes();
        } catch (IOException ioException) {
            logger.error("Unable to load resource: {}", resourceName, ioException);
        }
        return null;
    }

    protected String processTemplate(final String template, final Map<String, String> forReplace) {
        if (template == null) {
            return null;
        }

        var buf = template;
        for (var entry : forReplace.entrySet()) {
            buf = buf.replaceAll("\\$\\{" + entry.getKey() + "\\}", entry.getValue());
        }

        return buf;
    }

    protected String extractContextPath(final String fullPath, final int length) {
        if (fullPath == null) {
            return null;
        }

        URL url;
        try {
            url = fullPath.startsWith("htt") ? new URL(fullPath) : new URL("http://localhost" + fullPath);
        } catch (MalformedURLException malformedURLException) {
            logger.error("Unable to parse path", malformedURLException);
            return null;
        }

        var path = cleanPath(url.getPath());
        var parts = path.split(PATH_DELIMITER);
        if (parts.length > length) {
            var ref = Arrays.copyOf(parts, length);
            return String.join(PATH_DELIMITER, ref);
        }

        return path;
    }

    private String cleanPath(final String path) {
        return path.startsWith(PATH_DELIMITER) ? path.substring(PATH_DELIMITER.length()) : path;
    }
}
