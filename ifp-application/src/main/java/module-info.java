module madzi.apps.ifp {
    requires jdk.httpserver;

    requires org.slf4j;
    requires org.slf4j.simple;

    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.web;

    requires com.fasterxml.jackson.dataformat.xml;
    requires com.fasterxml.jackson.databind;
    requires com.ctc.wstx;

    exports madzi.apps.ifp;

    opens madzi.apps.ifp.view to javafx.fxml, javafx.web;
    opens madzi.apps.ifp.model to javafx.base, com.fasterxml.jackson.databind;
    opens madzi.apps.ifp.model.xml to com.fasterxml.jackson.databind;
    opens madzi.apps.ifp.model.fx to javafx.base;
}
