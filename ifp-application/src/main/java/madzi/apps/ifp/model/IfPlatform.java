package madzi.apps.ifp.model;

public enum IfPlatform {
    HTML(null),
    TWINE(HTML),
    INSTORY(HTML),
    PROTOPARSER(HTML),
    IFML_2(null),
    IFML_3(null);

    private IfPlatform(final IfPlatform parent) {
        this.parent = parent;
    }

    private final IfPlatform parent;

    public IfPlatform getParent() {
        return parent;
    }

    public boolean isRoot() {
        return parent == null;
    }
}
