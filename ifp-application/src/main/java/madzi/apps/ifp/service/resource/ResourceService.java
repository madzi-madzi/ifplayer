package madzi.apps.ifp.service.resource;

import java.nio.file.Path;
import java.util.UUID;

import madzi.apps.ifp.service.ApplicationService;

public interface ResourceService extends ApplicationService {

    void registerResource(UUID resourceId, Path resourcePath);

    byte[] loadResource(UUID resourceId, String pathToResource);

}
