package madzi.apps.ifp.service;

public enum Environment {
    INSTANCE;

    private final Context context = new ApplicationContext();

    public Context getContext() {
        return context;
    }
}
