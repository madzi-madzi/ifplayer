package madzi.apps.ifp.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ApplicationContext implements Context {

    private static final String APP_EXT = ".ifp";

    private final Path rootPath;
    private final Map<Class<? extends ApplicationService>, ApplicationService> services = new HashMap<>();

    public ApplicationContext() {
        this(Paths.get(System.getProperty("user.home"), APP_EXT));
    }

    public ApplicationContext(final Path path) {
        Objects.requireNonNull(path, "Application path cannot be NULL");

        this.rootPath = path;

        checkFolder(rootPath());
    }

    @Override
    public Path rootPath() {
        return rootPath;
    }

    public Path configPath() {
        return rootPath.resolve("ifp.cfg");
    }

    @Override
    public Path folder(final String folder) {
        Objects.requireNonNull(folder, "Requested folder can't be NULL");

        var path = rootPath.resolve(folder);
        checkFolder(path);
        return path;
    }

    @Override
    public void registerService(Class<? extends ApplicationService> clazz, ApplicationService service) {
        services.put(clazz, service);
    }

    @Override
    public <T> T getService(Class<T> clazz) {
        return (T) services.get(clazz);
    }

    @Override
    public void init() {
        services.values().forEach(ApplicationService::init);
    }

    @Override
    public void stop() {
        services.values().forEach(ApplicationService::stop);
    }

    private void checkFolder(final Path path) {
        var file = path.toFile();
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
