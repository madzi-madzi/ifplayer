package madzi.apps.ifp.view;

import java.net.URL;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * Controller for {@code html.fxml}.
 */
public class HtmlView implements Initializable {

    /** The logger. */
    private static final Logger logger = LoggerFactory.getLogger(HtmlView.class);

    @FXML
    private WebView webView;

    private WebEngine webEngine;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.info("Initializing Html View...");

        webEngine = webView.getEngine();
        webView.setContextMenuEnabled(false);
        // TODO load resouce
        
    }
}
