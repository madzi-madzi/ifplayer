package madzi.apps.ifp.service;

import java.nio.file.Path;

/**
 * Context for execution.
 */
public interface Context {

    /**
     * Returns the folder for application.
     *
     * @return the folder
     */
    Path rootPath();

    /**
     * Returns the folder inside the folder for application.
     *
     * @param folder requested folder
     * @return the folder
     */
    Path folder(String folder);

    /**
     * Register new service into service registry.
     *
     * @param clazz the class of service (usually interface)
     * @param service the service
     */
    void registerService(Class<? extends ApplicationService> clazz, ApplicationService service);

    /**
     * Returns the service from registry.
     *
     * @param <T> the requested service type
     * @param clazz the class of the requested service
     * @return the service
     */
    <T> T getService(Class<T> clazz);

    /**
     * Initializes all services into the registry.
     */
    void init();

    /**
     * Finalizes all services into the registry.
     */
    void stop();
}
