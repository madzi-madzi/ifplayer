package madzi.apps.ifp.model.xml;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import madzi.apps.ifp.model.IfLanguage;
import madzi.apps.ifp.model.IfPlatform;
import madzi.apps.ifp.model.StoryInfo;

@JacksonXmlRootElement(localName = "story-info")
public class XmlStoryInfo implements StoryInfo {

    @JacksonXmlProperty(isAttribute = true, localName = "version")
    private String infoVersion;
    @JacksonXmlProperty(localName = "ifid")
    private UUID ifid;
    @JacksonXmlProperty(localName = "title")
    private String title;
    @JacksonXmlProperty(localName = "version")
    private String version;
    @JacksonXmlProperty(localName = "date")
    private String date;
    @JacksonXmlElementWrapper(localName = "authors")
    @JacksonXmlProperty(localName = "author")
    private Set<String> authors;
    @JacksonXmlProperty(localName = "description")
    private String description;
    @JacksonXmlProperty(localName = "language")
    private IfLanguage language;
    @JacksonXmlProperty(localName = "platform")
    private IfPlatform platform;
    @JacksonXmlElementWrapper(localName = "tags")
    @JacksonXmlProperty(localName = "tag")
    private Set<String> tags;
    @JacksonXmlProperty(localName = "entry-point")
    private String start;
    @JacksonXmlProperty(localName = "story-conver")
    private String cover;

    @Override
    public UUID getIfId() {
        return ifid;
    }

    public void setIfId(final UUID ifid) {
        this.ifid = ifid;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    @Override
    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    @Override
    public Set<String> getAuthors() {
        return authors == null ? Collections.emptySet() : Collections.unmodifiableSet(authors);
    }

    public void setAuthors(final Set<String> authors) {
        this.authors = authors; // TODO fix collections problem
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public IfLanguage getLanguage() {
        return language;
    }

    public void setLanguage(final IfLanguage language) {
        this.language = language;
    }

    @Override
    public IfPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(final IfPlatform platform) {
        this.platform = platform;
    }

    @Override
    public Set<String> getTags() {
        return tags == null ? Collections.emptySet() : Collections.unmodifiableSet(tags);
    }

    public void setTags(final Set<String> tags) {
        this.tags = tags; //TODO fix collections problem
    }

    @Override
    public String getStart() {
        return start;
    }

    public void setStart(final String start) {
        this.start = start;
    }

    @Override
    public String getCover() {
        return cover;
    }

    public void setCover(final String cover) {
        this.cover = cover;
    }
}
