package madzi.apps.ifp.model;

import java.nio.file.Path;
import java.util.Objects;
import java.util.UUID;

public class StoryHandle {

    private final StoryInfo info;
    private final Path path;

    public StoryHandle(final StoryInfo info, final Path path) {
        Objects.requireNonNull(info, "The story information cannot be NULL");
        Objects.requireNonNull(path, "The story path cannot be NULL");

        this.info = info;
        this.path = path;
    }

    public StoryInfo getInfo() {
        return info;
    }

    public Path getPath() {
        return path;
    }

    public UUID getId() {
        return info.getIfId();
    }
}
