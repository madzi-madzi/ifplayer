package madzi.apps.ifp.view;

import java.io.IOException;
import java.util.Objects;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import madzi.apps.ifp.i18n.Messages;

public enum ViewManager {
    LIBRARY("library"),
    HTML("html");

    private ViewManager(final String viewName) {
        Objects.requireNonNull(viewName, "The view name should be NULL");
        this.viewName = "/view/" + viewName + ".fxml";
    }

    private final String viewName;

    public Parent getView() {
        try {
            return (Parent) FXMLLoader.load(ViewManager.class.getResource(viewName), Messages.resourceBundle());
        } catch (IOException ioException) {
            throw new ViewControllerException("Unable to load view: " + viewName, ioException);
        }
    }
}
