package madzi.apps.ifp.model;

import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import madzi.apps.ifp.model.xml.XmlStoryInfo;

@DisplayName("Info")
public class LoadSaveInfoTest {

    private static final String TITLE = "Story Title";
    private static final String VERSION = "0.7.3.1 alpha";
    private static final String TAG = "the tag";

    private static final String XML_STRING = String.join("\n", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
            "<story-info version=\"1.0\">",
            "<title>" + TITLE + "</title>",
            "<version>" + VERSION + "</version>",
            "<tags>",
            "<tag>" + TAG + "</tag>",
            "</tags>",
            "</story-info>");

    @Test
    @DisplayName("should be able load info from string")
    void testLoadInfo() throws IOException {
        var mapper = new XmlMapper();
        var info = mapper.readValue(XML_STRING, XmlStoryInfo.class);
        Assertions.assertNotNull(info);
        Assertions.assertEquals(TITLE, info.getTitle());
        Assertions.assertEquals(VERSION, info.getVersion());
        Assertions.assertNotNull(info.getTags());
        Assertions.assertTrue(info.getTags().contains(TAG));
    }

}
