package madzi.apps.ifp.view;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import madzi.apps.ifp.model.fx.FxStoryInfo;
import madzi.apps.ifp.service.Environment;
import madzi.apps.ifp.service.library.LibraryService;

public class LibraryView implements Initializable {

    private static final Logger logger = LoggerFactory.getLogger(LibraryView.class);

    private LibraryService libraryService;

    @FXML
    private TableView<FxStoryInfo> tableView;
    @FXML
    private TableColumn<FxStoryInfo, String> tcLanguage;
    @FXML
    private TableColumn<FxStoryInfo, String> tcPlatform;
    @FXML
    private TableColumn<FxStoryInfo, String> tcTitle;
    @FXML
    private TableColumn<FxStoryInfo, String> tcVersion;
    @FXML
    private TableColumn<FxStoryInfo, String> tcDate;
    @FXML
    private TableColumn<FxStoryInfo, Set<String>> tcAuthors;
    @FXML
    private TableColumn<FxStoryInfo, String> tcDescription;
    @FXML
    private TableColumn<FxStoryInfo, Set<String>> tcTags;

    @FXML
    public void onRowClick(final MouseEvent event) {
        if (event.getClickCount() == 2) {
            logger.info("Clicked:: {}", tableView.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    public void onExitAction() {
        logger.info("Exit event detected");
        Platform.exit();
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        logger.info("Start initialization Main view...");
        libraryService = Environment.INSTANCE.getContext().getService(LibraryService.class);

        tcLanguage.setCellValueFactory(new PropertyValueFactory<>("language"));
        tcPlatform.setCellValueFactory(new PropertyValueFactory<>("platform"));
        tcTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
        tcVersion.setCellValueFactory(new PropertyValueFactory<>("version"));
        tcDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        tcAuthors.setCellValueFactory(new PropertyValueFactory<>("authors"));
        tcDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        tcTags.setCellValueFactory(new PropertyValueFactory<>("tags"));

        var stories = FXCollections.observableArrayList(libraryService.findAll().stream()
                .map(FxStoryInfo::new)
                .collect(Collectors.toSet()));
        tableView.setItems(stories);
    }
}
