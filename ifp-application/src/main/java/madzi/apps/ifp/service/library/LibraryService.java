package madzi.apps.ifp.service.library;

import java.util.Set;

import madzi.apps.ifp.model.StoryInfo;
import madzi.apps.ifp.service.ApplicationService;

public interface LibraryService extends ApplicationService {

    Set<StoryInfo> findAll();
}
