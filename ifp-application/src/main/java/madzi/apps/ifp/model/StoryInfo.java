package madzi.apps.ifp.model;

import java.util.Set;
import java.util.UUID;

public interface StoryInfo {

    UUID getIfId();

    String getTitle();

    String getVersion();

    String getDate();

    Set<String> getAuthors();

    String getDescription();

    IfLanguage getLanguage();

    IfPlatform getPlatform();

    Set<String> getTags();

    String getStart();

    String getCover();
}
