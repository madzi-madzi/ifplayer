package madzi.apps.ifp.service.web.handler;

import java.io.IOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import madzi.apps.ifp.service.resource.ResourceService;

public class ResourceHttpHandler extends AbstractHttpHandler implements HttpHandler {

    private static final Logger logger = LoggerFactory.getLogger(ResourceHttpHandler.class);

    private ResourceService resourceService;

    public ResourceHttpHandler(final ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @Override
    public void handle(final HttpExchange exchange) throws IOException {
        var method = exchange.getRequestMethod();
        var path = exchange.getRequestURI().getPath();
        logger.info("Requested '{}' resource: {}", method, path);
        var response = resourceService.loadResource(getResourceId(path), getInternalPath(path));
        if (response != null) {
            exchange.sendResponseHeaders(SC_OK, response.length);
        } else {
            response = loadPageNotFound();
            exchange.sendResponseHeaders(SC_NOT_FOUND, response.length);
        }
        try (var outputStream = exchange.getResponseBody()) {
            outputStream.write(response);
        }
    }

    private String getInternalPath(final String path) {
        var fullPath = extractContextPath(path, 1_000);
        var context = extractContextPath(path, 1);
        return fullPath.substring(context.length() + 1);
    }

    private UUID getResourceId(final String path) {
        return UUID.fromString(extractContextPath(path, 1));
    }
}
