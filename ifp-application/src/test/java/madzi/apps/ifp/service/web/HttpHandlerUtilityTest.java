package madzi.apps.ifp.service.web;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sun.net.httpserver.HttpExchange;

import madzi.apps.ifp.service.web.handler.AbstractHttpHandler;

@DisplayName("Http Handler")
public class HttpHandlerUtilityTest extends AbstractHttpHandler {

    @Test
    @DisplayName("should be able extract context path")
    void testExctractContextPath() {
        var uuId = UUID.randomUUID().toString();
        var path = "/" + uuId + "/context/path/index.html";

        var resourceId = extractContextPath(path, 1);
        var context = extractContextPath(path, 2);

        Assertions.assertEquals(String.valueOf(uuId), resourceId);
        Assertions.assertEquals(uuId + "/context", context);
    }

    @Test
    @DisplayName("should be able process templates")
    void testTemplateProcessing() {
        var tpl = "Hello ${world}";
        var map =  Map.of("world", "Amigo");

        var result = processTemplate(tpl, map);

        Assertions.assertEquals("Hello Amigo", result);
    }

    @Override
    public void handle(final HttpExchange exchange) throws IOException {
    }
}
