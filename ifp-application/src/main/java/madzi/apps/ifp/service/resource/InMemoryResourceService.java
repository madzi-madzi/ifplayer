package madzi.apps.ifp.service.resource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InMemoryResourceService implements ResourceService {

    private static final Logger logger = LoggerFactory.getLogger(ResourceService.class);

    private Map<UUID, Path> resources = new HashMap<>();

    @Override
    public void init() {
        logger.info("Init resource service");
    }

    @Override
    public void stop() {
        logger.info("Stop resource service");
        // TODO Auto-generated method stub
    }

    @Override
    public void registerResource(UUID resourceId, Path resourcePath) {
        resources.put(resourceId, resourcePath);
    }

    @Override
    public byte[] loadResource(UUID resourceId, String pathToResource) {
        if (resources.containsKey(resourceId)) {
            var file = resources.get(resourceId).toFile();
            try (ZipFile zipFile = new ZipFile(file)) {
                var entry = zipFile.getEntry(pathToResource);
                return zipFile.getInputStream(entry).readAllBytes();
            } catch (ZipException zipException) {
                logger.error("Unable to load resource: {}", pathToResource, zipException);
            } catch (IOException ioException) {
                logger.error("Unable to locate file {}", file, ioException);
            }
        }
        return null;
    }
}
