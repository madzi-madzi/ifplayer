package madzi.apps.ifp.service.library;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctc.wstx.api.WstxInputProperties;
import com.ctc.wstx.api.WstxOutputProperties;
import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import madzi.apps.ifp.model.StoryHandle;
import madzi.apps.ifp.model.StoryInfo;
import madzi.apps.ifp.model.xml.XmlStoryInfo;

public class LocalLibraryService implements LibraryService {

    private final static Logger logger = LoggerFactory.getLogger(LibraryService.class);

    private final static String META_NAME = "story-info.xml";

    private final Path libPath;

    private Map<UUID, StoryHandle> handles = new HashMap<>();

    public LocalLibraryService(final Path libPath) {
        logger.info("Create library for folder: {}", libPath);
        Objects.requireNonNull(libPath, "The library path cannot be NULL");

        this.libPath = libPath;
    }

    @Override
    public Set<StoryInfo> findAll() {
        logger.info("Request list of all stories");
        return handles.values()
                .stream()
                .map(handle -> handle.getInfo())
                .collect(Collectors.toSet());
    }

    @Override
    public void init() {
        logger.info("Init library service");
        loadStories();
    }

    @Override
    public void stop() {
        logger.info("Stop library service");
        // Nothing to do
    }

    private void loadStories() {
        logger.info("Scan stories into folder: {}", libPath);
        try {
            Files.walkFileTree(libPath, new FileVisitor<Path>() {
    
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
    
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    logger.info(" * found {}", file);
                    var story = file.toFile();
                    if (story.exists() && story.isFile() && isAcceptable(story.getName().toLowerCase())) {
                        processFile(file);
                    }
                    return FileVisitResult.CONTINUE;
                }
    
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
    
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ioException) {
            logger.error("Unable to locate stories into {}", libPath, ioException);
        }
    }

    private void processFile(final Path path) {
        logger.info("   acceptable, processing...");

        try (var zipFile = new ZipFile(path.toFile())) {
            var zipEntry = zipFile.getEntry(META_NAME);
            if (zipEntry != null) {
                var storyInfo = loadInfo(zipFile.getInputStream(zipEntry).readAllBytes());
                if (storyInfo != null) {
                    var storyHandle = new StoryHandle(storyInfo, path);
                    handles.put(storyHandle.getId(), storyHandle);
                } else {
                    logger.error("Unable to load metainformation: {}", path);
                }
            }
        } catch (IOException ioException) {
            logger.error("Unable to load metainformation from file: {}", path, ioException);
        }

        logger.info("--- done");
    }

    private StoryInfo loadInfo(final byte[] bytes) {
        logger.info("Extract INFO from: {}", new String(bytes));
        if (bytes == null) {
            return null;
        }

        try {
            var inFactory = new WstxInputFactory();
            inFactory.setProperty(WstxInputProperties.P_MAX_ATTRIBUTE_SIZE, 32000);
            var outFactory = new WstxOutputFactory();
            outFactory.setProperty(WstxOutputProperties.P_OUTPUT_CDATA_AS_TEXT, true);
            var xmlFactory = new XmlFactory(inFactory, outFactory);

            var mapper = new XmlMapper(xmlFactory);
            return mapper.readValue(bytes, XmlStoryInfo.class);
        } catch (IOException ioException) {
            logger.error("Unable to parse loaded info", ioException);
        }

        return null;
    }

    private boolean isAcceptable(final String fileName) {
        return fileName.endsWith(".pif")
                || fileName.endsWith(".ifp")
                || fileName.endsWith(".zip")
                || fileName.endsWith(".ifz");
    }
}
