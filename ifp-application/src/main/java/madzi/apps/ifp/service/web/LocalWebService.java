package madzi.apps.ifp.service.web;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.spi.LocaleServiceProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.HttpServer;

import madzi.apps.ifp.service.resource.ResourceService;
import madzi.apps.ifp.service.web.handler.ResourceHttpHandler;

public class LocalWebService implements WebService {

    private static final Logger logger = LoggerFactory.getLogger(WebService.class);

    private final ResourceService resourceService;

    private final HttpServer httpServer;

    public LocalWebService(final ResourceService resourceService) {
        this(resourceService, 37080);
    }

    public LocalWebService(final ResourceService resourceService, final Integer port) {
        this.resourceService = resourceService;
        try {
            this.httpServer = HttpServer.create(new InetSocketAddress(port), 0);
            httpServer.createContext("/", new ResourceHttpHandler(resourceService));
        } catch (IOException ioException) {
            throw new RuntimeException("Unable to start web service", ioException);
        }
    }

    @Override
    public void init() {
        logger.info("Init web service");
        httpServer.start();
    }

    @Override
    public void stop() {
        logger.info("Stop web service");
        httpServer.stop(5);
    }
}
