package madzi.apps.ifp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import madzi.apps.ifp.i18n.Messages;
import madzi.apps.ifp.service.Context;
import madzi.apps.ifp.service.Environment;
import madzi.apps.ifp.service.library.LibraryService;
import madzi.apps.ifp.service.library.LocalLibraryService;
import madzi.apps.ifp.service.resource.InMemoryResourceService;
import madzi.apps.ifp.service.resource.ResourceService;
import madzi.apps.ifp.service.web.LocalWebService;
import madzi.apps.ifp.service.web.WebService;
import madzi.apps.ifp.view.ViewManager;

/**
 * Main class for IF Player Application;
 */
public class IfpApp extends Application {

    private static final Logger logger = LoggerFactory.getLogger(IfpApp.class);

    private final Context context = Environment.INSTANCE.getContext();

    public IfpApp() {
        var libraryService = new LocalLibraryService(context.folder("stories"));
        var resourceService = new InMemoryResourceService();
        var webService = new LocalWebService(resourceService);

        context.registerService(LibraryService.class, libraryService);
        context.registerService(ResourceService.class, resourceService);
        context.registerService(WebService.class, webService);
    }

    /**
     * Program entry point.
     *
     * @param args program arguments
     */
    public static void main(final String... args) {
        logger.info("{} {}", Messages.APP_NAME, Messages.APP_VERSION);
        logger.info("Start with argument: {}", String.join(";", args));
        launch(IfpApp.class, args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(ViewManager.LIBRARY.getView(), 800.0, 600.0));
        primaryStage.setTitle(Messages.APP_TITLE.message());
        primaryStage.getIcons().add(new Image(IfpApp.class.getClassLoader().getResourceAsStream("ifp.png")));

        primaryStage.show();
    }

    @Override
    public void init() {
        logger.info("Initialize...");
        context.init();
    }

    @Override
    public void stop() {
        logger.info("finalize...");
        context.stop();
    }
}
